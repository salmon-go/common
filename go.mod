module gitee.com/salmon-go/common

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cast v1.3.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.3
)
