package log

import (
	"testing"
	"time"
)

func Test_Init(t *testing.T) {
	Init(TraceLevel, WithLogPath("./logfile/"))
	for {
		Trace("......test trace")
		Debug("......test debug")
		Info("......test info")
		Warning("......test warning")
		Error("......test error")
		time.Sleep(1 * time.Second)
	}
}
