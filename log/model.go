package log

import "time"

type RotationOption func(*RotationSetting)

type RotationSetting struct {
	//level LogLevel
	path   string
	format string
	size   int64
	time   time.Duration
	age    time.Duration
}

//LogLevel 0:trace; 10:debug; 20:info; 30:warning; 40:error;
type LogLevel int

//0:trace; 10:debug; 20:info; 30:warning; 40:error;
const (
	TraceLevel LogLevel = iota * 10
	DebugLevel
	InfoLevel
	WarningLevel
	ErrorLevel
)
