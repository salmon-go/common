package extension

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	sgcdatabase "gitee.com/salmon-go/common/database"
	"github.com/gin-gonic/gin"
)

type testTable struct {
	sgcdatabase.TableBase
}

func Test_BindParam(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := CreateTestContext(w)

	c.Request, _ = http.NewRequest("POST", "/", bytes.NewBufferString(`{"id":"123456"}`))
	c.Request.Header.Add("Content-Type", "application/json") // set fake content-type

	context := NewContext(c)
	tt := testTable{}
	context.BindParam(&tt)
	fmt.Println(tt)
}

func CreateTestContext(w http.ResponseWriter) (c *gin.Context, r *gin.Engine) {
	r = gin.New()
	c = &gin.Context{}
	// c.reset()
	// c.writermem.reset(w)
	return
}
