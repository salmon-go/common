package extension

import (
	sgchttp "gitee.com/salmon-go/common/http"
	"github.com/gin-gonic/gin"
)

type scgContext struct {
	*gin.Context
}

func NewContext(c *gin.Context) (r *scgContext) {
	return &scgContext{c}
}

func (c *scgContext) BindParam(tableStruct interface{}) (err error) {
	err = c.Bind(&tableStruct)
	if err != nil {
		sgchttp.Error400(c.Context, err, "参数错误")
	}
	return
}
