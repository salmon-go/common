package utility

import "testing"

func Test_SliceStringContains(t *testing.T) {
	type testCase struct {
		array  []string
		value  string
		result bool
	}

	testGroup := map[string]testCase{
		"case1": {[]string{"a", "b", "c"}, "a", true},
		"case2": {[]string{"a", "b", "c"}, "d", false},
		"case3": {[]string{"a", "bb", "ccc"}, "ccc", true},
		"case4": {[]string{"a", "bb", "ccc"}, "c", false},
	}

	for k, v := range testGroup {
		t.Run(k, func(t *testing.T) {
			result := SliceStringContains(v.array, v.value)
			if result != v.result {
				t.Fatalf(k + "：测试结果与预期不符")
			}
		})
	}
}
