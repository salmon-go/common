package utility

//SliceStringContains 字符串切片s是否包含字符串i
func SliceStringContains(s []string, i string) bool {
	for _, v := range s {
		if v == i {
			return true
		}
	}
	return false
}
