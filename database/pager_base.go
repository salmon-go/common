package database

//PagerResult 分页查询返回值
type PagerResult struct {
	//Data      []*TableBase //不能放这里，基类类型不包含子类字段
	Total     int64 `json:"total"`
	PageSize  int   `json:"pagesize"`
	PageIndex int   `json:"pageindex"`
}

//PagerQuery 分页查询参数
type PagerQuery struct {
	PageSize  int `json:"pagesize"`
	PageIndex int `json:"pageindex"`
}

//NewPagerResult ...
func NewPagerResult(q PagerQuery) (r PagerResult) {
	return PagerResult{
		PageIndex: q.PageIndex,
		PageSize:  q.PageSize,
	}
}
