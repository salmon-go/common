package database

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	sql := "root:123456@tcp(localhost:3306)/task"
	InitGorm(sql)
	codeExit := m.Run()
	os.Exit(codeExit)
}
