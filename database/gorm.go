package database

import (
	"database/sql"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"

	sgclog "gitee.com/salmon-go/common/log"
)

var dbGorm *gorm.DB

func InitGorm(strConnection string) *gorm.DB {
	db, err := sql.Open("mysql", strConnection)
	if err != nil {
		sgclog.Info("连接Mysql数据库失败：" + err.Error())
		return nil
	}
	dbGorm, err = gorm.Open(mysql.New(mysql.Config{Conn: db}),
		&gorm.Config{
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
		})
		
	if err != nil {
		sgclog.Info("Grom初始化失败：" + err.Error())
		return nil
	}

	if dbGorm.Error != nil {
		sgclog.Info("Grom.Error：" + dbGorm.Error.Error())
		return nil
	}

	return dbGorm
}
