package database

type TaskInfo struct {
	Name        string `json:"name" gorm:"column:name"`
	Group       string `json:"group"`
	Cron        string `json:"cron"`
	RequestType string `json:"request_type"`
	TimeOut     int64  `json:"time_out"`
	//目前状态：0停止；10启动
	Status int    `json:"status"`
	Url    string `json:"url"`
	Body   string `json:"body"`
	TableBase
}

func (*TaskInfo) TableName() string {
	return "task_info"
}
