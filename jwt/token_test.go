package jwt

import (
	"testing"
	"time"
)

var strSecretKey = "testing"

//包含 EncryToken 和 DecrypToken
func Test_Token_Base(t *testing.T) {
	testGroup_Base := map[string]TokenUserInput{
		"case1": {ID: 1, Username: "aaaa", Password: "1111", Expire: 60},
		"case2": {ID: 2, Username: "!@#$%", Password: "^&*()", Expire: 300},
		"case3": {ID: 3, Username: "用户名", Password: "密码", Expire: 3600},
	}

	for k, v := range testGroup_Base {
		t.Run(k, func(t *testing.T) {
			token, err1 := EncryToken(v, strSecretKey)
			claim, err2 := DecrypToken(token, strSecretKey)
			if err1 != nil || err2 != nil {
				t.Fatalf(k + "：测试方法执行中报错")
			}
			if v.ID != claim.ID || v.Username != claim.Username || v.Password != claim.Password {
				t.Fatalf(k + "：测试结果与预期不符")
			}
		})
	}
}

func Test_Token_Expired(t *testing.T) {
	type testCase struct {
		input   TokenUserInput
		wait    int //设置等待时间超过 input.Expire 时，应该报错：ErrorMessage_ValidationErrorExpired
		message string
	}
	testGroup := map[string]testCase{
		"case1": {
			input:   TokenUserInput{ID: 1, Username: "username", Password: "password", Expire: 3},
			wait:    0,
			message: "",
		},
		"case2": {
			input:   TokenUserInput{ID: 1, Username: "username", Password: "password", Expire: 3},
			wait:    4,
			message: ErrorMessage_ValidationErrorExpired,
		},
	}
	for k, v := range testGroup {
		t.Run(k, func(t *testing.T) {
			token, err := EncryToken(v.input, strSecretKey)
			time.Sleep(time.Duration(v.wait) * time.Second)
			claims, err := DecrypToken(token, strSecretKey)
			if v.message != "" && claims != nil {
				//message不为空，应该报错，不应该能获取到claims
				t.Fatalf(k + "：测试结果与预期不符")
			}
			if err != nil && err.Error() != v.message {
				//有err但是错误信息与预期不一致
				t.Fatalf(k + "：测试结果与预期不符")
			}
		})
	}
}
